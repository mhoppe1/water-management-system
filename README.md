# Sample Title

The hypothetical project for Assignment 2 of SE 329.

|Schedule|Start Week|Duration|End Week|
|--------|----------|--------|--------|
|Project Road Map creation|0|3|3|
|Hiring Engineers |2|6|8|
|Development Environment|4|4|8|
|Architecture|8|3|11|
|Web Development|11|8|19|
|Embedded Development|11|8|19|
|Database Development|19|8|27|
|Web Testing|19|12|31|
|Embedded Testing|19|12|31|
|Database Testing|27|12|39|
|Integration Testing|39|8|47|
|Device installation|47|6|53|
|Server Deployment|47|6|53|
|Documentation Write Up|53|7|60|
|Stability Monitoring|53|7|60|


![alt text](v1329.png)